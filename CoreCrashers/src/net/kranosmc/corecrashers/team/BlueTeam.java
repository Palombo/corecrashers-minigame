package net.kranosmc.corecrashers.team;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;

public class BlueTeam {
	
	public static List<String> members = new ArrayList<String>();
	
	public static int coreHealth = 500;
	
	public static boolean alive = true;

	public String getName() {
		return "Blue";
	}
	
	public static List<String> getMembers() {
		return members;
	}
	
	public static int getCoreHealth() {
		return coreHealth;
	}
	
	public static void removeCoreHealth(int amount) {
		coreHealth = coreHealth - amount;
	}


	public static ChatColor getColor() {
		return ChatColor.BLUE;
	}

}
