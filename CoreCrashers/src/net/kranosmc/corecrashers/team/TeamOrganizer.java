package net.kranosmc.corecrashers.team;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;

public class TeamOrganizer {

	public static ArrayList<String> nonTeamedPlayers = new ArrayList<String>();

	@SuppressWarnings("deprecation")
	public static void asignPlayers() {
		for (String s : nonTeamedPlayers) {
			if (nonTeamedPlayers.size() != 0) {
				Player player = Bukkit.getPlayer(s);

				if (Bukkit.getOnlinePlayers().length == 8) {
					if (YellowTeam.getMembers().size() < 2) {
						YellowTeam.getMembers().add(player.getName());
					} else if (BlueTeam.getMembers().size() < 2) {
						BlueTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 2) {
						GreenTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 2) {
						RedTeam.getMembers().add(player.getName());
					}
				} else if (Bukkit.getOnlinePlayers().length == 9) {
					if (RedTeam.getMembers().size() < 3) {
						RedTeam.getMembers().add(player.getName());
					} else if (BlueTeam.getMembers().size() < 2) {
						BlueTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 2) {
						GreenTeam.getMembers().add(player.getName());
					} else if (YellowTeam.getMembers().size() < 2) {
						YellowTeam.getMembers().add(player.getName());
					}
				} else if (Bukkit.getOnlinePlayers().length == 10) {
					if (GreenTeam.getMembers().size() < 2) {
						GreenTeam.getMembers().add(player.getName());
					} else if (YellowTeam.getMembers().size() < 3) {
						YellowTeam.getMembers().add(player.getName());
					} else if (BlueTeam.getMembers().size() < 2) {
						BlueTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 3) {
						RedTeam.getMembers().add(player.getName());
					}
				} else if (Bukkit.getOnlinePlayers().length == 11) {
					if (BlueTeam.getMembers().size() < 3) {
						BlueTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 3) {
						GreenTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 2) {
						RedTeam.getMembers().add(player.getName());
					} else if (YellowTeam.getMembers().size() < 3) {
						YellowTeam.getMembers().add(player.getName());
					}
				} else if (Bukkit.getOnlinePlayers().length == 12) {
					if (BlueTeam.getMembers().size() < 4) {
						BlueTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 3) {
						GreenTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 2) {
						RedTeam.getMembers().add(player.getName());
					} else if (YellowTeam.getMembers().size() < 3) {
						YellowTeam.getMembers().add(player.getName());
					}
				} else if (Bukkit.getOnlinePlayers().length == 13) {
					if (YellowTeam.getMembers().size() < 4) {
						YellowTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 4) {
						GreenTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 4) {
						RedTeam.getMembers().add(player.getName());
					} else if (BlueTeam.getMembers().size() < 4) {
						BlueTeam.getMembers().add(player.getName());
					} 
				} else if (Bukkit.getOnlinePlayers().length == 14) {
					if (YellowTeam.getMembers().size() < 4) {
						YellowTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 4) {
						GreenTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 4) {
						RedTeam.getMembers().add(player.getName());
					} else if (BlueTeam.getMembers().size() < 4) {
						BlueTeam.getMembers().add(player.getName());
					} 
				} else if (Bukkit.getOnlinePlayers().length == 15) {
					if (BlueTeam.getMembers().size() < 4) {
						BlueTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 4) {
						GreenTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 4) {
						RedTeam.getMembers().add(player.getName());
					} else if (YellowTeam.getMembers().size() < 4) {
						YellowTeam.getMembers().add(player.getName());
					}
				} else if (Bukkit.getOnlinePlayers().length == 16) {
					if (BlueTeam.getMembers().size() < 4) {
						BlueTeam.getMembers().add(player.getName());
					} else if (GreenTeam.getMembers().size() < 4) {
						GreenTeam.getMembers().add(player.getName());
					} else if (RedTeam.getMembers().size() < 4) {
						RedTeam.getMembers().add(player.getName());
					} else if (YellowTeam.getMembers().size() < 4) {
						YellowTeam.getMembers().add(player.getName());
					}
				}
			} else {
				return;
			}
		}
	}

}
