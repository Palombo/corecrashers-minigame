package net.kranosmc.corecrashers.team;

import java.util.ArrayList;
import java.util.List;

import org.bukkit.ChatColor;

public class Spectators {

	public static List<String> members = new ArrayList<String>();

	public String getName() {
		return "Spectators";
	}

	public static List<String> getMembers() {
		return members;
	}

	public static ChatColor getColor() {
		return ChatColor.GRAY;
	}

}
