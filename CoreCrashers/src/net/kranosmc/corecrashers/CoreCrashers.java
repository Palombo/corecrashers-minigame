package net.kranosmc.corecrashers;

import java.security.acl.Permission;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.command.CommandExecutor;
import org.bukkit.entity.Player;
import org.bukkit.event.Listener;
import org.bukkit.plugin.java.JavaPlugin;

import com.turqmelon.MelonPerms.users.User;

import net.kranosmc.corecrashers.command.GameCMD;
import net.kranosmc.corecrashers.command.TestCMD;
import net.kranosmc.corecrashers.event.ChatListener;
import net.kranosmc.corecrashers.event.EntityListener;
import net.kranosmc.corecrashers.event.InventoryListener;
import net.kranosmc.corecrashers.event.JoinListener;
import net.kranosmc.corecrashers.event.PlayerListener;
import net.kranosmc.corecrashers.event.QuitListener;
import net.kranosmc.corecrashers.event.SneakListener;
import net.kranosmc.corecrashers.game.GameState;
import net.kranosmc.corecrashers.scoreboard.SBManager;
import net.kranosmc.corecrashers.shops.MikeTheMerchant;
import net.kranosmc.corecrashers.team.TeamOrganizer;
import net.kranosmc.corecrashers.util.AutoMessages;
import net.kranosmc.corecrashers.util.TimeManager;
import net.kranosmc.corecrashers.util.WorldManager;

public class CoreCrashers extends JavaPlugin {

	public static CoreCrashers instance;

	public static int ticks = 0;
	public static GameState gameState;

	@SuppressWarnings("deprecation")
	public void onEnable() {
		instance = this;

		gameState = GameState.LOBBY;

		register();

		for (Player player : Bukkit.getOnlinePlayers()) {
			player.setPlayerListName("§7" + player.getName());

			SBManager.loadBoard(player);
			SBManager.createColors();
			SBManager.createScoreMessages(player);

			TeamOrganizer.nonTeamedPlayers.add(player.getName());
		}

		for (World world : Bukkit.getServer().getWorlds()) {
			world.setTime(0);
		}

		Bukkit.getServer().getScheduler().runTaskTimer(this, new TimeManager(), 20L, 20L);
		Bukkit.getServer().getScheduler().runTaskTimer(this, new WorldManager(), 600L, 600L);
		Bukkit.getServer().getScheduler().runTaskTimer(this, new AutoMessages(), 20L * 30, 20L * 30);
	}

	public void onDisable() {

	}

	public void register() {
		registerListener(new JoinListener());
		registerListener(new ChatListener());
		registerListener(new EntityListener());
		registerListener(new InventoryListener());
		registerListener(new PlayerListener());
		registerListener(new WorldManager());
		registerListener(new QuitListener());
		registerListener(new SneakListener());
		registerListener(new MikeTheMerchant());

		registerCommand("test", new TestCMD());
		registerCommand("game", new GameCMD());
	}

	public void registerListener(Listener... listener) {
		for (Listener l : listener) {
			this.getServer().getPluginManager().registerEvents(l, this);
		}
	}

	public void registerCommand(String command, CommandExecutor commandExecutor) {
		this.getCommand(command).setExecutor(commandExecutor);
	}

	public static String formatTime(int secs) {
		int hours = secs / 3600;
		int remainder = secs % 3600;
		int minutes = remainder / 60;
		int seconds = remainder % 60;

		if (hours > 0) {
			return hours + ":" + minutes + ":" + seconds;
		} else if (minutes > 0) {
			if (seconds < 10) {
				return minutes + ":0" + seconds;
			} else {
				return minutes + ":" + seconds;
			}
		} else {
			return String.valueOf(seconds);
		}
	}

	public static int getTicks() {
		return ticks;
	}

	public static void setTicks(int ticks) {
		CoreCrashers.ticks = ticks;
	}

	public static CoreCrashers getInstance() {
		return instance;
	}
	
	/*
	 * if (Account.getRank(player.getUniqueId()) == Rank.OWNER) {
	 * player.setPlayerListName("§4§lO §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.HEADDEV) {
	 * player.setPlayerListName("§5§lHD §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.ADMIN) {
	 * player.setPlayerListName("§c§lA §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.DEV) {
	 * player.setPlayerListName("§d§lD §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.JRDEV) {
	 * player.setPlayerListName("§d§lJD §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.MODPLUS) {
	 * player.setPlayerListName("§9§lM+ §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.MOD) {
	 * player.setPlayerListName("§3§lM §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.HELPERPLUS) {
	 * player.setPlayerListName("§2§lH+ §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.HELPER) {
	 * player.setPlayerListName("§a§lH §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.BUILDER) {
	 * player.setPlayerListName("§e§lB §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.YOUTUBER) {
	 * player.setPlayerListName("§f§lYT §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.GOD) {
	 * player.setPlayerListName("§c§l❤  §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.MVP) {
	 * player.setPlayerListName("§d§l❤  §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.VIP) {
	 * player.setPlayerListName("§a§l❤  §7" + player.getName()); } else if
	 * (Account.getRank(player.getUniqueId()) == Rank.ALL) {
	 * player.setPlayerListName("§7" + player.getName()); } else {
	 * player.setPlayerListName("§7" + player.getName()); }
	 */

}
