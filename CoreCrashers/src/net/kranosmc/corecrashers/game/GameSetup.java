package net.kranosmc.corecrashers.game;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.block.Block;
import org.bukkit.entity.EntityType;
import org.bukkit.entity.Player;

import com.sk89q.worldedit.BlockVector;
import com.sk89q.worldedit.BlockWorldVector;
import com.sk89q.worldedit.LocalWorld;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.regions.Polygonal2DRegion;
import com.sk89q.worldguard.protection.regions.ProtectedPolygonalRegion;

@SuppressWarnings("deprecation")
public class GameSetup {

	 WorldEditPlugin we = (WorldEditPlugin) Bukkit.getPluginManager().getPlugin("WorldEdit");
	
	public static void setup(Player player) {
		buildWalls();
		spawnCrystals(player);
	}

	public static void buildWalls() {
		ProtectedPolygonalRegion wgRegion = null;
		LocalWorld world = null;
		
		Polygonal2DRegion weRegion = new Polygonal2DRegion(world, wgRegion.getPoints(), wgRegion.getMinimumPoint().getBlockY(), wgRegion.getMaximumPoint().getBlockY());
		
		for (BlockVector block : weRegion) {
		    Block bukkitBlock = BukkitUtil.toBlock(new BlockWorldVector(world, block));
		    // Do something with the block
		}
	}
	
	public static void spawnCrystals(Player player) {
		Location blueCoreLoc = new Location(Bukkit.getWorld("EggMap1"), -1182.415, 198.000, -2327.380);
		blueCoreLoc.getWorld().spawnEntity(blueCoreLoc, EntityType.ENDER_CRYSTAL);
		
	}

}
