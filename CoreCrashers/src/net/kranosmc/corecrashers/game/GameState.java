package net.kranosmc.corecrashers.game;

public enum GameState {
	LOBBY, COUNTDOWN, INGAME
}
