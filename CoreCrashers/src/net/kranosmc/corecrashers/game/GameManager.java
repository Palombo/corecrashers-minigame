package net.kranosmc.corecrashers.game;

import net.kranosmc.corecrashers.CoreCrashers;

public class GameManager {
	
	
	public static void setGameState(GameState gameState) {
		CoreCrashers.gameState = gameState;
	}
	
	public static GameState getGameState() {
		return CoreCrashers.gameState;
	}
	
	public static void startGame() {
		
	}
	
	public static void stopGame() {
		
	}

}
