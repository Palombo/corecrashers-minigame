package net.kranosmc.corecrashers.event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerQuitEvent;

public class QuitListener implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onQuit(PlayerQuitEvent event) {
		Player player = event.getPlayer();
		
		event.setQuitMessage("�3�l>> �c" + player.getName() + " has left the server. �7(" + (Bukkit.getServer().getOnlinePlayers().length - 1) + "/16)");
	}

}
