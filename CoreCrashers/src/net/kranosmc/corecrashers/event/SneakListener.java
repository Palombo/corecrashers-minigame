package net.kranosmc.corecrashers.event;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.Material;
import org.bukkit.World;
import org.bukkit.block.Block;
import org.bukkit.entity.Entity;
import org.bukkit.entity.Item;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerToggleSneakEvent;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;

import net.kranosmc.corecrashers.CoreCrashers;

@SuppressWarnings("deprecation")
public class SneakListener implements Listener {

	CuboidClipboard clipboard;

	public ArrayList<Block> getBlocks(Block start, int radius) {
		ArrayList<Block> blocks = new ArrayList<Block>();
		for (double x = start.getLocation().getX() - radius; x <= start.getLocation().getX() + radius; x++) {
			for (double y = start.getLocation().getY() - radius; y <= start.getLocation().getY() + radius; y++) {
				for (double z = start.getLocation().getZ() - radius; z <= start.getLocation().getZ() + radius; z++) {
					Location loc = new Location(start.getWorld(), x, y, z);
					blocks.add(loc.getBlock());
				}
			}
		}
		return blocks;
	}

	@EventHandler
	public void onPlayerToggleSneakEvent(PlayerToggleSneakEvent event) {
		Player player = event.getPlayer();

		if (player.isSneaking()) {

			Location loc = player.getLocation();
			loc.setY(loc.getBlockY() - 1);

			Block block = player.getLocation().getBlock();

			if (block.getType() == Material.WATER || block.getType() == Material.STATIONARY_WATER) {
				File dir = new File(CoreCrashers.getInstance().getDataFolder(),
						"/schematics/" + "CaptainHookCannon.schematic");

				EditSession editSession = new EditSession(new BukkitWorld(player.getLocation().getWorld()), 999999999);
				editSession.enableQueue();

				SchematicFormat schematic = SchematicFormat.getFormat(dir);
				try {
					clipboard = schematic.load(dir);
				} catch (DataException e1) {
					e1.printStackTrace();
				} catch (IOException e1) {
					e1.printStackTrace();
				}

				for (Block blocks : getBlocks(block, 3)) {
					if (blocks.getType() == Material.STAINED_CLAY || blocks.getType() == Material.STONE_BUTTON
							|| blocks.getType() == Material.DISPENSER || blocks.getType() == Material.WATER
							|| blocks.getType() == Material.getMaterial(44) || blocks.getType() == Material.SIGN
							|| blocks.getType() == Material.SIGN_POST || blocks.getType() == Material.WALL_SIGN
							|| blocks.getType() == Material.LEVER) {
						blocks.setType(Material.AIR);
					}
				}

				World world = Bukkit.getServer().getWorld(player.getWorld().getName());//get the world
				List<Entity> entList = world.getEntities();//get all entities in the world

				for(Entity current : entList){//loop through the list
					if (current instanceof Item){//make sure we aren't deleting mobs/players
						current.remove();//remove it
					}
				}

				clipboard.rotate2D(90);

				try {
					try {
						clipboard.saveSchematic(dir);
					} catch (IOException e) {
						e.printStackTrace();
					}
				} catch (com.sk89q.worldedit.world.DataException e) {
					e.printStackTrace();
				}

				try {
					clipboard.paste(editSession, BukkitUtil.toVector(block.getLocation()), true);
					player.sendMessage("�3�l>> �7Cannon rotated.");
				} catch (MaxChangedBlocksException e) {
					e.printStackTrace();
				}
				editSession.flushQueue();
			}
		}
	}
}
