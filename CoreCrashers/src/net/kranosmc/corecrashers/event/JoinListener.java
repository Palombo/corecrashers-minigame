package net.kranosmc.corecrashers.event;

import org.bukkit.Bukkit;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.PlayerJoinEvent;

import net.kranosmc.corecrashers.game.GameManager;
import net.kranosmc.corecrashers.game.GameState;
import net.kranosmc.corecrashers.lobby.TeamSelector;
import net.kranosmc.corecrashers.scoreboard.SBManager;
import net.kranosmc.corecrashers.team.TeamOrganizer;

public class JoinListener implements Listener {
	
	@SuppressWarnings("deprecation")
	@EventHandler
	public void onJoin(PlayerJoinEvent event) {
		Player player = event.getPlayer();
		
		SBManager.loadBoard(player);
		SBManager.createColors();
		SBManager.createScoreMessages(player);
		
		player.setPlayerListName("§7" + player.getName());
		
		if (GameManager.getGameState() == GameState.LOBBY) {
			TeamSelector.give(player);
		} else if (GameManager.getGameState() == GameState.INGAME) {
			//TODO: Make player a spectator
		}
		
		TeamOrganizer.nonTeamedPlayers.add(player.getName());
		
		event.setJoinMessage("§3§l>> §a" + player.getName() + " has joined the server. §7(" + Bukkit.getServer().getOnlinePlayers().length + "/16)");
	}

}
