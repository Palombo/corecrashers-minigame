package net.kranosmc.corecrashers.event;

import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.player.AsyncPlayerChatEvent;

import net.Kranos.Core.account.Account;
import net.kranosmc.corecrashers.team.BlueTeam;
import net.kranosmc.corecrashers.team.GreenTeam;
import net.kranosmc.corecrashers.team.RedTeam;
import net.kranosmc.corecrashers.team.YellowTeam;

public class ChatListener implements Listener {

	@EventHandler
	public void onChat(AsyncPlayerChatEvent event) {
		Player player = event.getPlayer();
		
		if (BlueTeam.members.contains(player.getName())) {
			event.setFormat(Account.getRank(player.getUniqueId()).getPrefix() + " �9" + player.getName() + " �8� �9" + event.getMessage());
		} else if (GreenTeam.members.contains(player.getName())) {
			event.setFormat(Account.getRank(player.getUniqueId()).getPrefix() + " �a" + player.getName() + " �8� �a" + event.getMessage());
		} else if (RedTeam.members.contains(player.getName())) {
			event.setFormat(Account.getRank(player.getUniqueId()).getPrefix() + " �c" + player.getName() + " �8� �c" + event.getMessage());
		} else if (YellowTeam.members.contains(player.getName())) {
			event.setFormat(Account.getRank(player.getUniqueId()).getPrefix() + " �e" + player.getName() + " �8� �e" + event.getMessage());
		} else {
			event.setFormat(Account.getRank(player.getUniqueId()).getPrefix() + " �7" + player.getName() + " �8� �7" + event.getMessage());
		}
	}

}
