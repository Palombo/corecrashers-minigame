package net.kranosmc.corecrashers.event;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.inventory.InventoryClickEvent;
import org.bukkit.event.inventory.InventoryInteractEvent;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.kranosmc.corecrashers.lobby.TeamSelector;
import net.kranosmc.corecrashers.team.BlueTeam;
import net.kranosmc.corecrashers.team.GreenTeam;
import net.kranosmc.corecrashers.team.RedTeam;
import net.kranosmc.corecrashers.team.Spectators;
import net.kranosmc.corecrashers.team.TeamOrganizer;
import net.kranosmc.corecrashers.team.YellowTeam;

public class InventoryListener implements Listener {

	@EventHandler
	public void onInventoryClick(InventoryClickEvent event) {
		Player player = (Player) event.getWhoClicked();
		
		//if (GameManager.getGameState() == GameState.LOBBY) event.setCancelled(true);

		if (!(event.getInventory().equals(TeamSelector.teamSelector)))
			return;
		if (!(event.getCurrentItem().hasItemMeta()))
			return;

		event.setCancelled(true);

		if (event.getCurrentItem().getItemMeta().getDisplayName()
				.contains(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blue Team")) {

			if (BlueTeam.getMembers().contains(player.getName())) {
				player.sendMessage("�3�l>> �7You are already on the �9�lBlue �7team.");
			} else {

				player.sendMessage("�3�l>> �7You have been added to the �9�lBlue �7team.");
				BlueTeam.getMembers().add(player.getName());
				player.setPlayerListName("�9" + player.getName());

				GreenTeam.getMembers().remove(player.getName());
				RedTeam.getMembers().remove(player.getName());
				YellowTeam.getMembers().remove(player.getName());
				Spectators.getMembers().remove(player.getName());
			}
		} else if (event.getCurrentItem().getItemMeta().getDisplayName()
				.contains(ChatColor.GREEN.toString() + ChatColor.BOLD + "Green Team")) {
			if (GreenTeam.getMembers().contains(player.getName())) {
				player.sendMessage("�3�l>> �7You are already on the �a�lGreen �7team.");
			} else {
				player.sendMessage("�3�l>> �7You have been added to the �a�lGreen �7team.");
				GreenTeam.getMembers().add(player.getName());
				player.setPlayerListName("�a" + player.getName());

				BlueTeam.getMembers().remove(player.getName());
				RedTeam.getMembers().remove(player.getName());
				YellowTeam.getMembers().remove(player.getName());
				Spectators.getMembers().remove(player.getName());
			}
		} else if (event.getCurrentItem().getItemMeta().getDisplayName()
				.contains(ChatColor.RED.toString() + ChatColor.BOLD + "Red Team")) {
			if (RedTeam.getMembers().contains(player.getName())) {
				player.sendMessage("�3�l>> �7You are already on the �c�lRed �7team.");
			} else {
				player.sendMessage("�3�l>> �7You have been added to the �c�lRed �7team.");
				RedTeam.getMembers().add(player.getName());
				player.setPlayerListName("�c" + player.getName());

				BlueTeam.getMembers().remove(player.getName());
				GreenTeam.getMembers().remove(player.getName());
				YellowTeam.getMembers().remove(player.getName());
				Spectators.getMembers().remove(player.getName());
			}
		} else if (event.getCurrentItem().getItemMeta().getDisplayName()
				.contains(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Yellow Team")) {
			if (YellowTeam.getMembers().contains(player.getName())) {
				player.sendMessage("�3�l>> �7You are already on the �e�lYellow �7team.");
			} else {
				player.sendMessage("�3�l>> �7You have been added to the �e�lYellow �7team.");
				player.setPlayerListName("�e" + player.getName());
				
				YellowTeam.getMembers().add(player.getName());

				BlueTeam.getMembers().remove(player.getName());
				GreenTeam.getMembers().remove(player.getName());
				RedTeam.getMembers().remove(player.getName());
				Spectators.getMembers().remove(player.getName());
			}
		}  else {
			return;
		}
		
		TeamOrganizer.nonTeamedPlayers.remove(player.getName());
		
		ItemStack blueTeam = new ItemStack(Material.STAINED_GLASS_PANE, BlueTeam.getMembers().size(), (short) 11);
		ItemMeta blueTeamMeta = blueTeam.getItemMeta();
		blueTeamMeta.setDisplayName(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blue Team �8(�7" + BlueTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> blueTeamLore = new ArrayList<String>();
		blueTeamLore.add("�7" + BlueTeam.getMembers().toString().replace("[", "").replace("]", ""));
		blueTeamMeta.setLore(blueTeamLore);
		blueTeam.setItemMeta(blueTeamMeta);
		TeamSelector.teamSelector.setItem(1, blueTeam);
		
		ItemStack greenTeam = new ItemStack(Material.STAINED_GLASS_PANE, GreenTeam.getMembers().size(), (short) 5);
		ItemMeta greenTeamMeta = greenTeam.getItemMeta();
		greenTeamMeta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Green Team �8(�7" + GreenTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> greenTeamLore = new ArrayList<String>();
		greenTeamLore.add("�7" + GreenTeam.getMembers().toString().replace("[", "").replace("]", ""));
		greenTeamMeta.setLore(greenTeamLore);
		greenTeam.setItemMeta(greenTeamMeta);
		TeamSelector.teamSelector.setItem(3, greenTeam);
		
		ItemStack redTeam = new ItemStack(Material.STAINED_GLASS_PANE, RedTeam.getMembers().size(), (short) 14);
		ItemMeta redTeamMeta = redTeam.getItemMeta();
		redTeamMeta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Red Team �8(�7" + RedTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> redTeamLore = new ArrayList<String>();
		redTeamLore.add("�7" + RedTeam.getMembers().toString().replace("[", "").replace("]", ""));
		redTeamMeta.setLore(redTeamLore);
		redTeam.setItemMeta(redTeamMeta);
		TeamSelector.teamSelector.setItem(5, redTeam);
		
		ItemStack yellowTeam = new ItemStack(Material.STAINED_GLASS_PANE, YellowTeam.getMembers().size(), (short) 4);
		ItemMeta yellowTeamMeta = yellowTeam.getItemMeta();
		yellowTeamMeta.setDisplayName(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Yellow Team �8(�7" + YellowTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> yellowTeamLore = new ArrayList<String>();
		yellowTeamLore.add("�7" + YellowTeam.getMembers().toString().replace("[", "").replace("]", ""));
		yellowTeamMeta.setLore(yellowTeamLore);
		yellowTeam.setItemMeta(yellowTeamMeta);
		TeamSelector.teamSelector.setItem(7, yellowTeam);
	}

	@EventHandler
	public void onInventoryInteract(InventoryInteractEvent event) {
		
		//if (GameManager.getGameState() == GameState.LOBBY) event.setCancelled(true);

		if (!(event.getInventory().equals(TeamSelector.teamSelector)))
			return;

		event.setCancelled(true);
	}

}
