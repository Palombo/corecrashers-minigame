package net.kranosmc.corecrashers.event;

import java.io.File;
import java.io.IOException;
import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.entity.FoodLevelChangeEvent;
import org.bukkit.event.player.PlayerDropItemEvent;
import org.bukkit.event.player.PlayerInteractEvent;
import org.bukkit.plugin.Plugin;

import com.sk89q.worldedit.CuboidClipboard;
import com.sk89q.worldedit.EditSession;
import com.sk89q.worldedit.MaxChangedBlocksException;
import com.sk89q.worldedit.WorldEdit;
import com.sk89q.worldedit.bukkit.BukkitUtil;
import com.sk89q.worldedit.bukkit.BukkitWorld;
import com.sk89q.worldedit.bukkit.WorldEditPlugin;
import com.sk89q.worldedit.data.DataException;
import com.sk89q.worldedit.schematic.SchematicFormat;

import net.kranosmc.corecrashers.CoreCrashers;
import net.kranosmc.corecrashers.game.GameManager;
import net.kranosmc.corecrashers.game.GameState;
import net.kranosmc.corecrashers.lobby.TeamSelector;
import net.kranosmc.corecrashers.shops.MikeTheMerchant;

@SuppressWarnings("deprecation")
public class PlayerListener implements Listener {

	public WorldEdit getWorldEdit() {
		Plugin p = Bukkit.getServer().getPluginManager().getPlugin("WorldEdit");
		if (p instanceof WorldEditPlugin)
			return (WorldEdit) p;
		else
			return null;
	}

	CuboidClipboard clipboard;

	@EventHandler
	public void onPlayerInteract(PlayerInteractEvent event)
			throws MaxChangedBlocksException, DataException, IOException {
		Player player = event.getPlayer();

		if (player.getItemInHand().getType().equals(Material.BLAZE_ROD)) {
			if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)
					|| event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
				MikeTheMerchant.spawn();
			}
		}

		if (player.getItemInHand().getType().equals(Material.MONSTER_EGG)) {
			if (!(player.getItemInHand().hasItemMeta()))
				return;
			if (event.getAction().equals(Action.RIGHT_CLICK_BLOCK)
					|| event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {
				if (player.getItemInHand().getItemMeta().getDisplayName()
						.contains(ChatColor.GRAY.toString() + ChatColor.BOLD + "Captain Hook Cannon Spawner")) {
					/*
					 * Bukkit.dispatchCommand(Bukkit.getConsoleSender(),
					 * "scl load CaptainHookCannon EggMap1 " +
					 * event.getClickedBlock().getLocation().getBlockX() + " " +
					 * event.getClickedBlock().getLocation().getBlockY() + " " +
					 * event.getClickedBlock().getLocation().getBlockZ());
					 */

					try {
						File dir = new File(CoreCrashers.getInstance().getDataFolder(),
								"/schematics/" + "CaptainHookCannon.schematic");

						EditSession editSession = new EditSession(new BukkitWorld(player.getLocation().getWorld()),
								999999999);
						editSession.enableQueue();

						SchematicFormat schematic = SchematicFormat.getFormat(dir);
						clipboard = schematic.load(dir);

						clipboard.paste(editSession, BukkitUtil.toVector(player.getLocation()), true);
						editSession.flushQueue();
					} catch (DataException | IOException ex) {
						ex.printStackTrace();
					} catch (MaxChangedBlocksException ex) {
						ex.printStackTrace();
					}
					// Schematic.paste("CaptainHookCannon.schematic",
					// player.getLocation());
					// Schematic.save(player, "Test");

					if (player.getItemInHand().getAmount() == 1) {
						player.getInventory().remove(player.getItemInHand());
					} else {
						player.getItemInHand().setAmount(player.getItemInHand().getAmount() - 1);
					}
					player.sendMessage("�3�l>> �7You have spawned a �6�lCaptain Hook Cannon�7.");
				}
			}
		}

		if (event.getAction().equals(Action.RIGHT_CLICK_AIR) || event.getAction().equals(Action.RIGHT_CLICK_BLOCK)
				|| event.getAction().equals(Action.LEFT_CLICK_AIR)
				|| event.getAction().equals(Action.LEFT_CLICK_BLOCK)) {

			if (!(player.getItemInHand().hasItemMeta()))
				return;

			if (player.getItemInHand().getItemMeta().getDisplayName()
					.contains(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Team Selector")) {

				TeamSelector.open(player);
			}
		}

	}

	@EventHandler
	public void onFoodLevelChange(FoodLevelChangeEvent event) {
		if (GameManager.getGameState() == GameState.LOBBY)
			event.setCancelled(true);
	}

	@EventHandler
	public void onDrop(PlayerDropItemEvent event) {
		// if (GameManager.getGameState() == GameState.LOBBY)
		// event.setCancelled(true);
	}

}
