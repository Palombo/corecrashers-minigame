package net.kranosmc.corecrashers.command;

import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.Kranos.Core.account.Account;
import net.Kranos.Core.account.Rank;
import net.Kranos.Core.messages.M;
import net.kranosmc.corecrashers.cannons.CaptainHookCannon;

public class GameCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("game")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;

				if (Account.canDo(player.getUniqueId(), Rank.DEV) == true) {
					if (args.length < 1) {
						CaptainHookCannon.giveSpawner(player, 1);
						showHelp(sender);
					} else {
						if (args[0].equalsIgnoreCase("start")) {
							//TODO: Start game
							Bukkit.broadcastMessage("Start Game");
						} else if (args[0].equalsIgnoreCase("stop")) {
							//TODO: Stop game
							Bukkit.broadcastMessage("Stop Game");
						} else {
							showHelp(sender);
						}
					}
				} else if (Account.canDo(player.getUniqueId(), Rank.DEV) == false) {
					sender.sendMessage(M.neededRank(Rank.DEV));
				}
			}
		}
		return true;
	}

	public void showHelp(CommandSender sender) {
		sender.sendMessage("�3�l>> �b/game [start/stop]");
	}
}