package net.kranosmc.corecrashers.command;

import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

import net.kranosmc.corecrashers.team.BlueTeam;
import net.kranosmc.corecrashers.team.GreenTeam;
import net.kranosmc.corecrashers.team.RedTeam;
import net.kranosmc.corecrashers.team.Spectators;
import net.kranosmc.corecrashers.team.YellowTeam;

public class TestCMD implements CommandExecutor {

	public boolean onCommand(CommandSender sender, Command cmd, String label, String[] args) {
		if (cmd.getName().equalsIgnoreCase("test")) {
			if (sender instanceof Player) {
				Player player = (Player) sender;
			
				if (args[0].equalsIgnoreCase("blue")) {
					player.sendMessage("Blue Team");
					BlueTeam.getMembers().add(player.getName());
					
					GreenTeam.getMembers().remove(player.getName());
					RedTeam.getMembers().remove(player.getName());
					YellowTeam.getMembers().remove(player.getName());
					Spectators.getMembers().remove(player.getName());
				} else if (args[0].equalsIgnoreCase("green")) {
					player.sendMessage("Green Team");
					GreenTeam.getMembers().add(player.getName());
					
					BlueTeam.getMembers().remove(player.getName());
					RedTeam.getMembers().remove(player.getName());
					YellowTeam.getMembers().remove(player.getName());
					Spectators.getMembers().remove(player.getName());
				} else if (args[0].equalsIgnoreCase("red")) {
					player.sendMessage("Red Team");
					RedTeam.getMembers().add(player.getName());
					
					BlueTeam.getMembers().remove(player.getName());
					GreenTeam.getMembers().remove(player.getName());
					YellowTeam.getMembers().remove(player.getName());
					Spectators.getMembers().remove(player.getName());
				} else if (args[0].equalsIgnoreCase("yellow")) {
					player.sendMessage("Yellow Team");
					YellowTeam.getMembers().add(player.getName());
					
					BlueTeam.getMembers().remove(player.getName());
					GreenTeam.getMembers().remove(player.getName());
					RedTeam.getMembers().remove(player.getName());
					Spectators.getMembers().remove(player.getName());
				} else if (args[0].equalsIgnoreCase("spectators")) {
					player.sendMessage("Spectators Team");
					Spectators.getMembers().add(player.getName());
					
					BlueTeam.getMembers().remove(player.getName());
					GreenTeam.getMembers().remove(player.getName());
					RedTeam.getMembers().remove(player.getName());
					YellowTeam.getMembers().remove(player.getName());
				}
			}
		}
		return true;
	}
}