package net.kranosmc.corecrashers.cannons;

import java.util.ArrayList;

import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class CaptainHookCannon {
	
	public static void giveSpawner(Player player, int amount) {
		ItemStack spawner = new ItemStack(Material.MONSTER_EGG, amount);
		ItemMeta spawnerMeta = spawner.getItemMeta();
		spawnerMeta.setDisplayName(ChatColor.GRAY.toString() + ChatColor.BOLD + "Captain Hook Cannon Spawner");
		ArrayList<String> spawnerLore = new ArrayList<String>();
		spawnerLore.add("�7Right click to spawn a Captain Hook Cannon.");
		spawnerMeta.setLore(spawnerLore);
		spawner.setItemMeta(spawnerMeta);
		player.getInventory().addItem(spawner);
	}

}
