package net.kranosmc.corecrashers.util;

import org.bukkit.Bukkit;

public class AutoMessages implements Runnable {
	
	int messageNumber = 10;

	@Override
	public void run() {
		if (messageNumber == 10) {
			messageNumber = 9;
			Bukkit.broadcastMessage("�c�l>> �6You are allowed to build cannons on the cannon building island off your core's island.");
		} else if (messageNumber == 9) {
			messageNumber = 8;
			Bukkit.broadcastMessage("�c�l>> �6Cannons are also purchasble in the in-game shop via the Villager.");
		} else if (messageNumber == 8) {
			messageNumber = 7;
			Bukkit.broadcastMessage("�c�l>> �6You may by items from any villager around the map with EXP.");
		} else if (messageNumber == 7) {
			messageNumber = 6;
			Bukkit.broadcastMessage("�c�l>> �6There is a zombie spawner at every island to gain EXP. 1 zombie kill is 1 EXP.");
		} else if (messageNumber == 6) {
			messageNumber = 5;
			Bukkit.broadcastMessage("�c�l>> �6Killing a player will earn you 5 EXP.");
		} else if (messageNumber == 5) {
			messageNumber = 4;
			Bukkit.broadcastMessage("�c�l>> �6You can rotate any cannon by crouching in the water.");
		} else if (messageNumber == 4) {
			messageNumber = 3;
			Bukkit.broadcastMessage("Message 4");
		} else if (messageNumber == 3) {
			messageNumber = 2;
			Bukkit.broadcastMessage("Message 3");
		} else if (messageNumber == 2) {
			messageNumber = 1;
			Bukkit.broadcastMessage("Message 2");
		} else if (messageNumber == 1) {
			messageNumber = 10;
			Bukkit.broadcastMessage("Message 1");
		}
	}

}
