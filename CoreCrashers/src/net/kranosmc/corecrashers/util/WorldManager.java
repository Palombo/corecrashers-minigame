package net.kranosmc.corecrashers.util;

import org.bukkit.Bukkit;
import org.bukkit.World;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.weather.WeatherChangeEvent;

public class WorldManager implements Runnable, Listener {
	
	@Override
	public void run() {
		for (World world : Bukkit.getServer().getWorlds()) {
			world.setTime(0);
		}
	}
	
	@EventHandler
	public void onWeatherChange(WeatherChangeEvent event) {
		event.setCancelled(true);
	}

}
