package net.kranosmc.corecrashers.util;

import org.bukkit.Bukkit;
import org.bukkit.Sound;
import org.bukkit.entity.Player;

import net.kranosmc.corecrashers.CoreCrashers;
import net.kranosmc.corecrashers.game.GameManager;
import net.kranosmc.corecrashers.game.GameState;

public class TimeManager implements Runnable {

	public final int lobbySeconds = 90;
	public final int minPlayers = 8;

	@SuppressWarnings("deprecation")
	@Override
	public void run() {

		if (CoreCrashers.getTicks() > 0) {
			CoreCrashers.setTicks(CoreCrashers.getTicks() - 1);
		}

		if (GameManager.getGameState() == GameState.LOBBY) {

			int online = Bukkit.getServer().getOnlinePlayers().length;

			if ((CoreCrashers.getTicks() > 0 && CoreCrashers.getTicks() <= 10)
					|| CoreCrashers.getTicks() > 0 && CoreCrashers.getTicks()%30==0) {
				for (Player player : Bukkit.getOnlinePlayers()) {
					player.sendMessage(" ");
					player.sendMessage("§3§l>> §7The game will start in §c§l" + CoreCrashers.formatTime(CoreCrashers.getTicks()) + "§7...");
					player.sendMessage(" ");
					player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
				}
			}

			if (CoreCrashers.getTicks() == 0) {
				if (online == minPlayers) {
					Bukkit.broadcastMessage("Game Starting...");
					GameManager.setGameState(GameState.INGAME);

				} else {
					CoreCrashers.setTicks(lobbySeconds);
					for (Player player : Bukkit.getOnlinePlayers()) {
						player.sendMessage("§3§l>> §7There must be at least §c§l" + minPlayers + " §7players for the game to start.");
						player.playSound(player.getLocation(), Sound.CLICK, 1, 1);
					}
				}
			}
		}
	}

}
