package net.kranosmc.corecrashers.scoreboard;

import java.util.ArrayList;
import java.util.HashMap;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.entity.Player;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scoreboard.DisplaySlot;
import org.bukkit.scoreboard.Objective;
import org.bukkit.scoreboard.Scoreboard;
import org.bukkit.scoreboard.Team;

import net.kranosmc.corecrashers.CoreCrashers;
import net.kranosmc.corecrashers.team.BlueTeam;
import net.kranosmc.corecrashers.team.GreenTeam;
import net.kranosmc.corecrashers.team.RedTeam;
import net.kranosmc.corecrashers.team.Spectators;
import net.kranosmc.corecrashers.team.YellowTeam;

public class SBManager {

	@SuppressWarnings("deprecation")
	public void loadScoreboards() {
		for (Player p : Bukkit.getOnlinePlayers()) {
			loadBoard(p);
		}
	}

	@SuppressWarnings("deprecation")
	public static void loadBoard(final Player p) {
		final Scoreboard board = Bukkit.getScoreboardManager().getNewScoreboard();

		final Objective objectives = board.registerNewObjective("test", "dummy");
		objectives.setDisplaySlot(DisplaySlot.SIDEBAR);


		ArrayList<ChatColor> colors = createColors();

		final ArrayList<Team> teams = new ArrayList<Team>();
		final HashMap<Team, ChatColor> teamColor = new HashMap<Team, ChatColor>();

		for (int i = 0; i < 13; i++) {
			final Team team = board.registerNewTeam("row" + Integer.toString(i));
			ChatColor chatColor = colors.get(0);
			colors.remove(chatColor);

			team.addPlayer(Bukkit.getOfflinePlayer(chatColor.toString()));
			objectives.getScore(chatColor.toString()).setScore(i);
			teams.add(team);
			teamColor.put(team, chatColor);
		}

		objectives.setDisplayName(ChatColor.DARK_AQUA.toString() +  ChatColor.BOLD + "Core Crashers §8(§c" + Bukkit.getServer().getOnlinePlayers().length + "/16§8)");

		new BukkitRunnable() {

			@Override
			public void run() {
				ArrayList<String> newMsgs = createScoreMessages(p);

				for (Team team : teams) {
					try {
						int line = teams.size() - teams.indexOf(team) -1;
						String message = newMsgs.get(line);
						team.setPrefix(message);
						objectives.getScore(teamColor.get(team).toString()).setScore(teams.indexOf(team));
					} catch (IndexOutOfBoundsException exception) {
						board.resetScores(teamColor.get(team).toString());
					}
				}
			}

		}.runTaskTimer(CoreCrashers.getInstance(), 15, 15);

		p.setScoreboard(board);
	}

	public static ArrayList<ChatColor> createColors() {
		ArrayList<ChatColor> colors = new ArrayList<ChatColor>();

		colors.add(ChatColor.BLACK);
		colors.add(ChatColor.WHITE);
		colors.add(ChatColor.DARK_GRAY);
		colors.add(ChatColor.DARK_RED);
		colors.add(ChatColor.RED);
		colors.add(ChatColor.BLUE);
		colors.add(ChatColor.GREEN);
		colors.add(ChatColor.YELLOW);
		colors.add(ChatColor.GOLD);
		colors.add(ChatColor.DARK_PURPLE);
		colors.add(ChatColor.GRAY);
		colors.add(ChatColor.LIGHT_PURPLE);
		colors.add(ChatColor.AQUA);
		colors.add(ChatColor.DARK_AQUA);

		return colors;
	}

	public static ArrayList<String> createScoreMessages(Player player) {
		ArrayList<String> scores = new ArrayList<String>();
		
		scores.add(" ");
		
		scores.add(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Your Team");
		if (BlueTeam.members.contains(player.getName())) {
			scores.add("  §9§lBlue");
		} else if (GreenTeam.members.contains(player.getName())) {
			scores.add("  §a§lGreen");
		} else if (RedTeam.members.contains(player.getName())) {
			scores.add("  §c§lRed");
		} else if (YellowTeam.members.contains(player.getName())) {
			scores.add("  §e§lYellow");
		} else if (Spectators.members.contains(player.getName())) {
			scores.add("  §7§lSpectators");
		} else {
			scores.add("  §7§lN/A");
		}
		
		scores.add(" ");
		
		scores.add(ChatColor.DARK_PURPLE.toString() + ChatColor.BOLD + "Countdown");
		if (CoreCrashers.getTicks() == 0) {
			scores.add("  §7§lN/A");
		} else {
			scores.add("  §7§l" + CoreCrashers.formatTime(CoreCrashers.getTicks()));
		}
		
		scores.add(" ");

		if (BlueTeam.alive == true) {
			scores.add("§7" + BlueTeam.getCoreHealth() + " §9§lBlue");	
		} else if (BlueTeam.alive == false) {
			scores.add("§7" + BlueTeam.getCoreHealth() + " §8§mBlue");
		}
		
		if (GreenTeam.alive == true) {
			scores.add("§7" + GreenTeam.getCoreHealth() + " §a§lGreen");	
		} else if (GreenTeam.alive == false) {
			scores.add("§7" + GreenTeam.getCoreHealth() + " §a§l§mGreen");
		}
		
		if (RedTeam.alive == true) {
			scores.add("§7" + RedTeam.getCoreHealth() + " §c§lRed");	
		} else if (RedTeam.alive == false) {
			scores.add("§7" + RedTeam.getCoreHealth() + " §c§l§mRed");
		}
		
		if (YellowTeam.alive == true) {
			scores.add("§7" + YellowTeam.getCoreHealth() + " §e§lYellow");	
		} else if (YellowTeam.alive == false) {
			scores.add("§7" + YellowTeam.getCoreHealth() + " §e§l§mYellow");
		}
		
		scores.add(" ");
		
		scores.add("§5kranosmc.net");

		return scores;
	}

}
