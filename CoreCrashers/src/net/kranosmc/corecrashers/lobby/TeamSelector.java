package net.kranosmc.corecrashers.lobby;

import java.util.ArrayList;

import org.bukkit.Bukkit;
import org.bukkit.ChatColor;
import org.bukkit.Material;
import org.bukkit.entity.Player;
import org.bukkit.inventory.Inventory;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

import net.kranosmc.corecrashers.team.BlueTeam;
import net.kranosmc.corecrashers.team.GreenTeam;
import net.kranosmc.corecrashers.team.RedTeam;
import net.kranosmc.corecrashers.team.YellowTeam;

public class TeamSelector {
	
	public static Inventory teamSelector;
	
	public static void give(Player player) {
		ItemStack teamSelector = new ItemStack(Material.NETHER_STAR);
		ItemMeta teamSelectorMeta = teamSelector.getItemMeta();
		teamSelectorMeta.setDisplayName(ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Team Selector");
		ArrayList<String> teamSelectorLore = new ArrayList<String>();
		teamSelectorLore.add("�f�l�nSelect a team");
		teamSelectorMeta.setLore(teamSelectorLore);
		teamSelector.setItemMeta(teamSelectorMeta);
		player.getInventory().setItem(0, teamSelector);
	}
	
	public static void open(Player player) {
		teamSelector = Bukkit.createInventory(player, 9, ChatColor.DARK_AQUA.toString() + ChatColor.BOLD + "Team Selector");

		for (int i = 0; i < teamSelector.getSize(); i++) {
			ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE);
			ItemMeta glassMeta = glass.getItemMeta();
			glassMeta.setDisplayName(ChatColor.WHITE.toString() + ChatColor.BOLD + "-");
			glass.setItemMeta(glassMeta);
			teamSelector.setItem(i, glass);
		}

		ItemStack blueTeam = new ItemStack(Material.STAINED_GLASS_PANE, BlueTeam.getMembers().size(), (short) 11);
		ItemMeta blueTeamMeta = blueTeam.getItemMeta();
		blueTeamMeta.setDisplayName(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blue Team �8(�7" + BlueTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> blueTeamLore = new ArrayList<String>();
		blueTeamLore.add("�7" + BlueTeam.getMembers().toString().replace("[", "").replace("]", ""));
		blueTeamMeta.setLore(blueTeamLore);
		blueTeam.setItemMeta(blueTeamMeta);
		teamSelector.setItem(1, blueTeam);
		
		ItemStack greenTeam = new ItemStack(Material.STAINED_GLASS_PANE, GreenTeam.getMembers().size(), (short) 5);
		ItemMeta greenTeamMeta = greenTeam.getItemMeta();
		greenTeamMeta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Green Team �8(�7" + GreenTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> greenTeamLore = new ArrayList<String>();
		greenTeamLore.add("�7" + GreenTeam.getMembers().toString().replace("[", "").replace("]", ""));
		greenTeamMeta.setLore(greenTeamLore);
		greenTeam.setItemMeta(greenTeamMeta);
		teamSelector.setItem(3, greenTeam);
		
		ItemStack redTeam = new ItemStack(Material.STAINED_GLASS_PANE, GreenTeam.getMembers().size(), (short) 14);
		ItemMeta redTeamMeta = redTeam.getItemMeta();
		redTeamMeta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Red Team �8(�7" + RedTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> redTeamLore = new ArrayList<String>();
		redTeamLore.add("�7" + RedTeam.getMembers().toString().replace("[", "").replace("]", ""));
		redTeamMeta.setLore(redTeamLore);
		redTeam.setItemMeta(redTeamMeta);
		teamSelector.setItem(5, redTeam);
		
		ItemStack yellowTeam = new ItemStack(Material.STAINED_GLASS_PANE, GreenTeam.getMembers().size(), (short) 4);
		ItemMeta yellowTeamMeta = yellowTeam.getItemMeta();
		yellowTeamMeta.setDisplayName(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Yellow Team �8(�7" + YellowTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> yellowTeamLore = new ArrayList<String>();
		yellowTeamLore.add("�7" + YellowTeam.getMembers().toString().replace("[", "").replace("]", ""));
		yellowTeamMeta.setLore(yellowTeamLore);
		yellowTeam.setItemMeta(yellowTeamMeta);
		teamSelector.setItem(7, yellowTeam);

		player.openInventory(teamSelector);
	}
	
	public static void update(Player player) {
		teamSelector = Bukkit.createInventory(player, 9, ChatColor.AQUA.toString() + ChatColor.BOLD + "Team Selector");

		for (int i = 0; i < teamSelector.getSize(); i++) {
			ItemStack glass = new ItemStack(Material.STAINED_GLASS_PANE);
			ItemMeta glassMeta = glass.getItemMeta();
			glassMeta.setDisplayName(ChatColor.WHITE.toString() + ChatColor.BOLD + "-");
			glass.setItemMeta(glassMeta);
			teamSelector.setItem(i, glass);
		}

		ItemStack blueTeam = new ItemStack(Material.STAINED_GLASS_PANE, BlueTeam.getMembers().size(), (short) 11);
		ItemMeta blueTeamMeta = blueTeam.getItemMeta();
		blueTeamMeta.setDisplayName(ChatColor.BLUE.toString() + ChatColor.BOLD + "Blue Team �8(�7" + BlueTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> blueTeamLore = new ArrayList<String>();
		blueTeamLore.add("�7" + BlueTeam.getMembers().toString().replace("[", "").replace("]", ""));
		blueTeamMeta.setLore(blueTeamLore);
		blueTeam.setItemMeta(blueTeamMeta);
		teamSelector.setItem(1, blueTeam);
		
		ItemStack greenTeam = new ItemStack(Material.STAINED_GLASS_PANE, GreenTeam.getMembers().size(), (short) 5);
		ItemMeta greenTeamMeta = greenTeam.getItemMeta();
		greenTeamMeta.setDisplayName(ChatColor.GREEN.toString() + ChatColor.BOLD + "Green Team �8(�7" + GreenTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> greenTeamLore = new ArrayList<String>();
		greenTeamLore.add("�7" + GreenTeam.getMembers().toString().replace("[", "").replace("]", ""));
		greenTeamMeta.setLore(greenTeamLore);
		greenTeam.setItemMeta(greenTeamMeta);
		teamSelector.setItem(3, greenTeam);
		
		ItemStack redTeam = new ItemStack(Material.STAINED_GLASS_PANE, GreenTeam.getMembers().size(), (short) 14);
		ItemMeta redTeamMeta = redTeam.getItemMeta();
		redTeamMeta.setDisplayName(ChatColor.RED.toString() + ChatColor.BOLD + "Red Team �8(�7" + RedTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> redTeamLore = new ArrayList<String>();
		redTeamLore.add("�7" + RedTeam.getMembers().toString().replace("[", "").replace("]", ""));
		redTeamMeta.setLore(redTeamLore);
		redTeam.setItemMeta(redTeamMeta);
		teamSelector.setItem(5, redTeam);
		
		ItemStack yellowTeam = new ItemStack(Material.STAINED_GLASS_PANE, GreenTeam.getMembers().size(), (short) 4);
		ItemMeta yellowTeamMeta = yellowTeam.getItemMeta();
		yellowTeamMeta.setDisplayName(ChatColor.YELLOW.toString() + ChatColor.BOLD + "Yellow Team �8(�7" + YellowTeam.getMembers().size() + "�8/�74�8)");
		ArrayList<String> yellowTeamLore = new ArrayList<String>();
		yellowTeamLore.add("�7" + YellowTeam.getMembers().toString().replace("[", "").replace("]", ""));
		yellowTeamMeta.setLore(yellowTeamLore);
		yellowTeam.setItemMeta(yellowTeamMeta);
		teamSelector.setItem(7, yellowTeam);
	}

}
